# TeiHeaderCahierToSpreadsheet

## Objectif

Convertir les métadonnées descriptives des headers d'un lot de fichiers XML TEI vers un format tableur.

### Pourquoi ?

- Vous disposez de nombreux fichiers XML TEI et vous souhaitez vérifier facilement la cohérence et qualité des métadonnées présentent dans ces fichiers.

- Vous souhaitez déposer ces fichiers XML TEI dans un entrepôt disposant d'un import par lot utilisant le format tableur pour l'envoi en masse.

## Comment ?

1- Lancer ce programme via myBinder [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fmnauge%2Fteiheadercahiertospreadsheet/master)

2- Uploader tous vos fichiers TEI à convertir dans le dossier datas/Tei/ 
(il y a déjà 2 XML d'exemple dans ce dossier. Pour un simple test vous pouvez aller directement à l'étape 3)

2- Lancer le notebook notebooks/TeiHeaderToSpreadsheet.ipynb

3- Cliquer sur Cell>Run All dans le menu en haut de page

4- Cliquer sur le bouton convert TEI en bas de page

5- Cliquer sur le lien généré pour ouvrir ou télécharger la version tableur

### Démonstration vidéo



<a href="https://videotheque.univ-poitiers.fr/embed.php?id=vqxf7hs1x3hasmfrmjcn&link=hmk41a6s79za8935lxnk3h8qjb7hpf" target="_blank"><img src="https://videotheque.univ-poitiers.fr/datas/thumbs/1597823428_teicahier2xls.jpg?173507581" 
alt="miniature video friction less data"/></a>

