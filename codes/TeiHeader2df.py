# -*- coding: utf-8 -*-
"""
Created on Tue Jul  7 10:23:22 2020

@author: mnauge
"""

import pandas as pd
import glob
import ntpath

import os
import sys
sys.path.insert(0, os.path.abspath('./../codes/'))
import TeiHeaderParsing as thp

from datetime import datetime


def teiHeader2dataFrameDcTerms(pathTeiFile):
    
    currentTei = thp.getHeaderSoupFromFile(pathTeiFile)
    
    # specifie columns names in a list
    columnsNamesDcTerms = []
    columnsNamesDcTerms.append('Title')
    columnsNamesDcTerms.append('Creator')
    columnsNamesDcTerms.append('Contributor')
    columnsNamesDcTerms.append('Publisher')
    columnsNamesDcTerms.append('Date Issued')
    columnsNamesDcTerms.append('Date Available')
    columnsNamesDcTerms.append('Identifier')
    columnsNamesDcTerms.append('Rights')
    columnsNamesDcTerms.append('Description')
    columnsNamesDcTerms.append('Source')
    columnsNamesDcTerms.append('Date Created')
    columnsNamesDcTerms.append('Language')
    columnsNamesDcTerms.append('Subject')
    columnsNamesDcTerms.append('file')
    
    # create empty dataframe with only columns names
    df = pd.DataFrame(columns=columnsNamesDcTerms)
    
    # create a dictionnary with 
    # key = column name
    # value = extracted values from TEI Header
    dicRow = {}
    dicRow['Title'] = thp.get_fileDesc_titleStmt_title(currentTei)
    dicRow['Creator'] = thp.get_fileDesc_titleStmt_author(currentTei)
    dicRow['Contributor'] =thp.get_fileDesc_titleStmt_editor(currentTei)
    dicRow['Publisher'] = thp.get_fileDesc_publicationStmt_publisher(currentTei)
    dicRow['Date Issued'] = thp.get_fileDesc_publicationStmt_date(currentTei)
    dicRow['Date Available' ] = thp.get_fileDesc_publicationStmt_date(currentTei)
    dicRow['Identifier'] = thp.get_fileDesc_publicationStmt_idno(pathTeiFile)
    dicRow['Rights'] = thp.get_fileDesc_publicationStmt_availability_licence(currentTei)
    dicRow['Description'] = thp.get_profileDesc_abstract(currentTei)
    
    # il y a 3 soucres possible en fonction du type de document
    # générique, monographie, manuscrit
    source1 = thp.get_fileDesc_sourceDesc_bibl(currentTei)
    if len(source1)>0:
        dicRow['Source'] = source1
    else:
        source2 = thp.get_fileDesc_sourceDesc_biblStruct_monogr_title(currentTei)
        if len(source2)>0:
            dicRow['Source'] = source2
        else:
            source3 = thp.get_fileDesc_sourceDesc_msDesc_msItem_title(currentTei)
            if len(source3)>0:
                dicRow['Source'] = source3
    
    
    dicRow['Date Created'] = thp.get_profileDesc_creation_date(currentTei)   
    dicRow['Language'] = thp.get_profileDesc_langUsage_language(currentTei)
    dicRow['Subject'] = thp.get_profileDesc_textClass_keywords_term(currentTei)
    
    teiFilename = ntpath.basename(pathTeiFile)
    dicRow['file'] = teiFilename

    # append this row to dataframe
    df = df.append(dicRow,ignore_index=True)
    
    return df


def massiveTeiToSpreadsheet(pthDirTei, pathDirSpreadOut):
    listTeiFile = glob.glob(pthDirTei+"*.xml")
    
    dfConcact = pd.DataFrame()

    for teiFile in listTeiFile:
        dfCur = teiHeader2dataFrameDcTerms(teiFile)
        
        if len(dfConcact)==0:
            dfConcact = dfCur.copy()
        else:
            dfConcact = pd.concat([dfConcact, dfCur])
            
    filenameDate = datetime.now().strftime("%d-%m-%Y_%I-%M-%S_%p")
    filenameOut = pathDirSpreadOut+filenameDate+".xlsx"
    dfConcact.to_excel(filenameOut)
    
    return filenameOut
    
    
    
    
    
    
    
    
    
    
    

    
