# -*- coding: utf-8 -*-
"""
Created on Tue Jul  6 08:42:33 2020

@author: mnauge
"""

from bs4 import BeautifulSoup


def getHeaderSoupFromFile(pathTeiFile):
    soup = None
    with open(pathTeiFile, encoding='utf-8') as x:        
        file = x.read()
        soup = BeautifulSoup(file,'html.parser')
        soup = soup.find('teiheader')
    return soup


def get_fileDesc_titleStmt_value(soup, key):
    outputValue = ""
    try:
        finded = soup.find('titlestmt').find(key)
        outputValue = finded.text
    except :
        pass
    return outputValue   


def get_fileDesc_publicationStmt_value(soup, key):
    outputValue = ""

    try:
        finded = soup.find('publicationstmt').find(key)
        outputValue = finded.text
    except :
        pass
    return outputValue


def get_profileDesc_value(soup, key):
    outputValue = ""

    try:
        finded = soup.find('profiledesc').find(key)
        outputValue = finded.text
    except :
        pass
    return outputValue


def get_fileDesc_titleStmt_title(soup):
    return get_fileDesc_titleStmt_value(soup, 'title')
            

def get_fileDesc_titleStmt_author(soup):
    return get_fileDesc_titleStmt_value(soup, 'author')
            

def get_fileDesc_titleStmt_editor(soup):
    return get_fileDesc_titleStmt_value(soup, 'editor')
            

def get_fileDesc_publicationStmt_publisher(soup):
    return get_fileDesc_publicationStmt_value(soup, 'publisher')
            

def get_fileDesc_publicationStmt_date(soup):
    return get_fileDesc_publicationStmt_value(soup, 'date')


def get_fileDesc_publicationStmt_idno(soup):
    return get_fileDesc_publicationStmt_value(soup, 'idno')


def get_fileDesc_publicationStmt_availability_licence(soup):
    outputValue = ""

    try:
        finded = soup.find('publicationstmt').find('availability').find('licence')
        outputValue = finded['target']
    except :
        pass
            
    return outputValue


def get_profileDesc_abstract(soup):
    return get_profileDesc_value(soup, 'abstract')



def get_fileDesc_sourceDesc_bibl(soup):
    outputValue = ""

    try:
        finded = soup.find('filedesc').find('sourcedesc').find('bibl')
        outputValue = finded.text
    except :
        pass
            
    return outputValue


def get_fileDesc_sourceDesc_biblStruct_monogr_title(soup):
    outputValue = ""

    try:
        finded = soup.find('filedesc').find('sourcedesc').find('biblstruct').find('monogr').find('title')
        outputValue = finded.text
    except :
        pass
            
    return outputValue



def get_fileDesc_sourceDesc_msDesc_msItem_title(soup):
    outputValue = ""

    try:
        finded = soup.find('filedesc').find('sourcedesc').find('msdesc').find('msItem').find('title')
        outputValue = finded.text
    except :
        pass
            
    return outputValue


def get_profileDesc_creation_date(soup):
    outputValue = ""

    try:
        finded = soup.find('profiledesc').find('creation').find('date')
        outputValue = finded['when']
    except :
        pass
    return outputValue


def get_profileDesc_creation_date_beforeAfter(soup):
    outputValue = ""
    # TODO 
    return outputValue

def get_profileDesc_langUsage_language(soup):
    outputValue = ""

    try:
        finded = soup.find('profiledesc').find('langusage').find('language')
        outputValue = finded['ident']
    except :
        pass
    return outputValue


def get_profileDesc_textClass_keywords_term(soup):
    outputValue = ""

    try:
        finded = soup.find('profiledesc').find('textclass').find('keywords').findAll('term',{"type":"subject"})
        
        for i, keyword in enumerate(finded):
            if i==0:
                outputValue = keyword.text
            else:
                outputValue = outputValue+" | "+ keyword.text

    except :
        pass
    return outputValue













